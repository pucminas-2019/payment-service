import os
import time
import random
from kombu import Connection, Exchange, Queue, Consumer, Producer
import logging


logging.basicConfig(filename='payment.log', level=logging.INFO)

#RABBITMQ_URL = os.environ.get("RABBITMQ_URL")
conn = Connection("rabbitmq")

exchange = Exchange("orders", type="direct")

queue = Queue(name="orders", exchange=exchange, routing_key="orders")
queue_payment = Queue(name="payment", exchange=exchange, routing_key="payment")

status_payment = [
    "Aprovado", 
    "Não Aprovado", 
    "Cancelado" 
]

def proccess_message(body, message):
    logging.info(f"receive payment from queue {body}")
    time.sleep(5)
    message.ack()
    if "status" not in body:
        body["status"] = random.choice(status_payment)
    producer(body)


def consumer(timeout=28):
    with Consumer(
        conn, 
        queues=queue, 
        callbacks=[proccess_message], 
        accept=["text/plain", "application/json"]
    ):
        while True:
            try:
                conn.drain_events(timeout=2)
            except Exception as e:
                time.sleep(timeout)




def producer(message):
    logging.info(f"Send payment to queue {message}")
    channel = conn.channel()
    producer = Producer(exchange=exchange, channel=channel, routing_key="payment")
    queue_payment.maybe_bind(conn)
    queue_payment.declare()
    producer.publish(message)

if __name__ == "__main__":
    consumer()
